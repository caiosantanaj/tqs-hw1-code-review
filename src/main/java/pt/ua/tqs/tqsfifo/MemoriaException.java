package pt.ua.tqs.tqsfifo;

public class MemoriaException extends Exception {
    
    public MemoriaException (String errorMsg) {
        super(errorMsg);
    }
    
    public MemoriaException (String errorMsg, Throwable cause) {
        super(errorMsg, cause);
    }
    
}
