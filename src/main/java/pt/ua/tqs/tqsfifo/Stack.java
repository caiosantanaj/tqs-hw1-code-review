package pt.ua.tqs.tqsfifo;

public class Stack<T> {

    private T[] pilha;
    private int pin;
    private int size;

    @SuppressWarnings("unchecked")
    public Stack(int size) {
        pilha = (T[]) new Object[size];
        this.size = size;
        pin = 0;
    }

    public void push(T e) throws MemoriaException {
        if (pin < size) {
            pilha[pin++] = e;
        } else {
            throw new MemoriaException("Stack full!");
        }
    }

    public T pop() throws MemoriaException {
        if (pilha.length == 0) {
            throw new MemoriaException("Stack empty!");
        }
        else {
            return pilha[--pin];
        }
    }

    public int length() {
        return pilha.length;
    }

    @Override
    public String toString() {
        
        StringBuilder sb = new StringBuilder();
        
        sb.append("Stack: ");

        for (int i = pin; i > 0; i--) {
            sb.append(pilha[i - 1]);
            
            if (i == 1)
                sb.append(".");
            else
                sb.append(",");
        }

        return sb.toString();
    }

}
