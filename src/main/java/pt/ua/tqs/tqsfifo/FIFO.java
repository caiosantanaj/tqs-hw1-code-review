package pt.ua.tqs.tqsfifo;

public class FIFO<T> {
    private int tailIndex;
    private int headIndex;
    private int maxSize;
    private int curSize;
    private boolean empty;
    private T[] queue;

    public FIFO(int size) {
        tailIndex = headIndex = curSize = 0;
        empty = true;
        queue = (T[]) new Object[size];
        maxSize = size;
    }

    public void enqueue(T value) throws MemoriaException {
        if (!isFull()) {
            queue[tailIndex] = value;
            tailIndex = (tailIndex + 1) % maxSize;
            empty = false;
            curSize++;
        }
        else {
            throw new MemoriaException("FIFO full!!!");
        }

    }

    public T dequeue() throws MemoriaException {
        if (!empty) {
            T value = queue[headIndex];
            queue[headIndex] = null;
            headIndex = (headIndex + 1) % maxSize;
            empty = headIndex == tailIndex;
            curSize--;
            return value;
        } 
        else {
            throw new MemoriaException("FIFO empty!!!");
        }
    }
    
    public boolean isEmpty() {
        return empty;
    }
    
    public boolean isFull() {
        return !empty && (tailIndex==headIndex);
    }
    
    public int size() {
        return curSize;
    }
    
    public T peek() {
        if (!empty) {
            return queue[headIndex];
        }
        return null;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i=0;i<queue.length;i++) {
            if (queue[i]!= null) {
                sb.append(queue[i]);
                sb.append(", ");
            }
        }
        int lastIndex = sb.lastIndexOf(", ");
        if (lastIndex == -1) {
            sb.append("]");
        }
        else {
            sb.replace(lastIndex, lastIndex+2, "]");
        }
        return sb.toString();
    }
}
